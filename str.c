#include "str.h"
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

String string_from(char *in) {
  string_size_t size = strlen(in);
  return string_from_len(in, size);
}

String string_from_len(char *in, string_size_t size) {
  String str;
  str.len = size;
  str.val = malloc(size + 1);
  memcpy(str.val, in, size);
  str.val[size] = '\0';
  return str;
}

String string_from_int(int in) {
  string_size_t size = (string_size_t)snprintf(NULL, 0, "%d", in);
  String str = string_of_len(size);
  sprintf(str.val, "%d", in);
  return str;
}

String string_of_len(string_size_t size) {
  String str;
  str.len = size;
  str.val = malloc(size + 1);
  for (string_size_t i = 0; i <= size; i++)
    str.val[i] = '\0';
  return str;
}

void string_tolower(String *str) {
  for (string_size_t i = 0; i < str->len; i++)
    str->val[i] = tolower(str->val[i]);
}

void string_append(String *str, String *other) {
  str->len = str->len + other->len;
  str->val = realloc(str->val, str->len + 1);
  strncat(str->val, other->val, other->len);
  str->val[str->len] = '\0';
}

void string_free(String *str) {
  if (str->val != NULL)
    free(str->val);
}
