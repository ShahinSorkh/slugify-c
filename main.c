#include "slug.h"
#include <stdio.h>

int main() {
  Container c = slugs_create(50, (map_size_t)300);

  slugs_add(&c, "Hello World!");
  slugs_add(&c, "Hello_World!");
  slugs_add(&c, "HelloWorld");
  slugs_add(&c, "Hello World!!!");
  slugs_add(&c, "Top 5 greatest's hits");
  slugs_add(&c, "Sh So");
  slugs_add(&c, "Foo Bar");
  slugs_add(&c, "NRGS");
  slugs_add(&c, "SHIT");
  slugs_add(&c, "SHITTY");
  slugs_add(&c, "SHoooY");
  slugs_add(&c, "YaY");
  slugs_add(&c, "yEeT");

  printf("Uniq Slugs: %u\n", map_count_filled(&c.uniqs));
  Slug *slug = c.slugs;
  for (size_t i = 0; i < c.last_ptr; i++) {
    printf("'%s' -> '%s'\n", slug->orig.val, slug->id.val);
    slug++;
  }

  slugs_free(&c);

  return 0;
}
