#ifndef STR_H_
#define STR_H_

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

typedef size_t string_size_t;
typedef struct string {
  string_size_t len;
  char *val;
} String;

String string_from(char *in);
String string_from_int(int in);
String string_from_len(char *in, string_size_t size);
String string_of_len(string_size_t size);
void string_append(String *str, String *other);
void string_tolower(String *str);
void string_free(String *str);

#endif // STR_H_
