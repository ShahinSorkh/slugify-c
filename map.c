#include "map.h"
#include <stddef.h>
#include <stdlib.h>

Map map_create(map_size_t size) {
  Map map;
  map.size = size;
  map.entries = malloc(sizeof(Entry) * size);

  Entry *entry = map.entries;
  for (map_size_t i = 0; i < size; i++) {
    entry->value = 0;
    entry->filled = false;
    entry++;
  }

  return map;
}

void map_free(Map *map) { free(map->entries); }

map_k map_key_from_str(Map *map, String *key) {
  unsigned long hash = 7;
  for (map_size_t i = 0; i < key->len; i++)
    hash = 31 * hash + key->val[i];

  return hash % map->size;
}

void map_set(Map *map, String *key, map_v val) {
  map_k k = map_key_from_str(map, key);
  if (k > map->size) {
    return;
  }

  Entry *entry = &map->entries[k];
  entry->filled = true;
  entry->value = val;
}

void map_uset(Map *map, String *key) {
  map_k k = map_key_from_str(map, key);
  if (k > map->size) {
    return;
  }

  map->entries[k].filled = false;
}

map_v map_get(Map *map, String *key) {
  map_k k = map_key_from_str(map, key);
  Entry *entry = &map->entries[k];

  if (k > map->size || !entry->filled) {
    return 0;
  }

  return entry->value;
}

map_v map_incr(Map *map, String *key) {
  map_v v = map_get(map, key);
  map_set(map, key, v + 1);
  return v;
}

unsigned int map_count_filled(Map *map) {
  unsigned int cnt = 0;
  Entry *entry = map->entries;
  for (map_size_t i = 0; i < map->size; i++) {
    if (entry->filled)
      cnt++;
    entry++;
  }
  return cnt;
}
