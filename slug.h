#ifndef SLUG_H_
#define SLUG_H_

#include "map.h"
#include "str.h"
#include <stdint.h>

typedef struct slug {
  String orig;
  String id;
} Slug;

typedef uint_least16_t container_size_t;
typedef struct container {
  Map uniqs;
  Slug *slugs;
  container_size_t last_ptr;
  container_size_t size;
} Container;

Container slugs_create(container_size_t size, map_size_t uniq_size);
void slugs_add(Container *container, char *slug_str);
void slugs_free(Container *c);

#endif // SLUG_H_
