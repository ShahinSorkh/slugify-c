all: clean build

build:
	gcc -O2 -Wall -Werror -o app main.c slug.c map.c str.c -lpcre2-8

build-dbg:
	gcc -g -Wall -Werror -o app.dbg main.c slug.c map.c str.c -lpcre2-8

run: build
	./app

debug-run: build-dbg
	gdb -ex run app.dbg

debug-mem: build-dbg
	valgrind --leak-check=full -s ./app.dbg

debug: build-dbg debug-run debug-mem

clean:
	rm -f app app.dbg
