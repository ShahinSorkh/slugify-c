#ifndef MAP_H_
#define MAP_H_

#include "str.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint_least8_t map_size_t;
typedef uint_least32_t map_k;
typedef uint_least8_t map_v;

typedef struct entry {
  map_v value;
  bool filled;
} Entry;

typedef struct map {
  map_size_t size;
  Entry *entries;
} Map;

Map map_create(map_size_t size);
void map_free(Map *map);

void map_set(Map *map, String *key, map_v val);
void map_unset(Map *map, String *key);

map_v map_get(Map *map, String *key);
map_v map_incr(Map *map, String *key);

unsigned int map_count_filled(Map *map);

#endif // MAP_H_
