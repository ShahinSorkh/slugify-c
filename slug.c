#define PCRE2_CODE_UNIT_WIDTH 8

#include "slug.h"
#include <pcre2.h>

String slugs_mkid(String *in) {
  int errornumber;
  PCRE2_SIZE erroroffset;

  pcre2_code *re1 =
      pcre2_compile((PCRE2_SPTR) "[^A-Za-z0-9]+", PCRE2_ZERO_TERMINATED, 0,
                    &errornumber, &erroroffset, NULL);
  pcre2_code *re2 =
      pcre2_compile((PCRE2_SPTR) "(^-)|(-$)", PCRE2_ZERO_TERMINATED, 0,
                    &errornumber, &erroroffset, NULL);

  String id_str = string_of_len(in->len);
  pcre2_substitute(re1, (PCRE2_SPTR)in->val, PCRE2_ZERO_TERMINATED, 0,
                   PCRE2_SUBSTITUTE_GLOBAL, NULL, NULL, (PCRE2_SPTR) "-",
                   PCRE2_ZERO_TERMINATED, (PCRE2_UCHAR *)id_str.val,
                   &id_str.len);
  pcre2_substitute(re2, (PCRE2_SPTR)id_str.val, PCRE2_ZERO_TERMINATED, 0,
                   PCRE2_SUBSTITUTE_GLOBAL, NULL, NULL, (PCRE2_SPTR) "",
                   PCRE2_ZERO_TERMINATED, (PCRE2_UCHAR *)id_str.val,
                   &id_str.len);
  string_tolower(&id_str);

  pcre2_code_free(re1);
  pcre2_code_free(re2);

  return id_str;
}

Container slugs_create(container_size_t size, map_size_t uniq_size) {
  Container c;
  c.uniqs = map_create(uniq_size);
  c.slugs = malloc(sizeof(Slug) * size);
  c.size = size;
  c.last_ptr = 0;
  return c;
}

void slugs_free(Container *c) {
  if (c->last_ptr > 0) {
    Slug *slug = c->slugs;
    for (container_size_t i = 0; i < c->last_ptr; i++) {
      free(slug->orig.val);
      free(slug->id.val);
      slug++;
    }
  }
  free(c->slugs);
  map_free(&c->uniqs);
}

void slugs_add(Container *c, char *orig) {
  if (c->size == c->last_ptr)
    return;

  String orig_str = string_from(orig);
  String id_str = slugs_mkid(&orig_str);

  int count = map_incr(&c->uniqs, &id_str);

  if (count > 0) {
    String dash = string_from("-");
    String count_str = string_from_int(count);

    string_append(&id_str, &dash);
    string_append(&id_str, &count_str);

    string_free(&dash);
    string_free(&count_str);
  }

  Slug slug;
  slug.orig = orig_str;
  slug.id = id_str;

  c->slugs[c->last_ptr] = slug;
  c->last_ptr++;
}
